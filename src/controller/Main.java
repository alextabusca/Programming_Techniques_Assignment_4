package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JOptionPane;

import gui.GraphicalUserInterface;
import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class Main {

    static GraphicalUserInterface frame;

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Bank bank = new Bank();
        // bank.setData(FileHandler.readInformation("bank.txt"));

        Person[] p = new Person[20];
        p[1] = new Person("Alex", 1);
        p[2] = new Person("Diana", 2);
        p[3] = new Person("Ana", 3);
        p[4] = new Person("Claudiu", 4);
        p[5] = new Person("Ionut", 5);
        p[6] = new Person("Bogdan", 6);
        p[7] = new Person("Mara", 7);
        p[8] = new Person("Stefan", 8);

        for (int i = 1; i <= 8; i++) {
            SavingAccount a1 = new SavingAccount(1, 100);
            SpendingAccount a2 = new SpendingAccount(2, 250);
            bank.addAccount(p[i], a1);
            bank.addAccount(p[i], a2);
        }

        FileHandler.writeInformation(bank.getBank(), "bank.txt");

        System.out.println(bank);

        GraphicalUserInterface frame = new GraphicalUserInterface();

        frame.addListPersonsListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("List of all persons");
                frame.buildPersonList(bank.getPersons());

            }

        });

        frame.addListAccountsListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("List of accounts for given person");
                frame.buildAccountList();

            }

        });

        frame.addListAccountsConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int personID = 0;
                try {
                    personID = Integer.valueOf(frame.txtPersonID.getText());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame, "Incorrect personID format", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                for (Person p : bank.getPersons()) {
                    if (p.getPersonID() == personID) {
                        System.out.println("Person found.");
                        for (Account acc : bank.getAccounts(p)) {
                            frame.buildAccountPanel(acc);
                            System.out.println(acc.toString());
                        }
                    }
                }
            }

        });

        frame.addAddPersonListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("Add Person");
                frame.buildAddPerson();
            }

        });

        frame.addAddPersonConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {

                String name;
                double money;
                String type;
                try {
                    name = frame.txtPersonName.getText();
                    money = Double.valueOf(frame.txtInitialAmount.getText());
                    type = frame.txtAccountType.getText();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int id = bank.getMaxPersonID() + 1;
                Person p = new Person(name, id);

                if (type.equals("spending")) {
                    SpendingAccount acc = new SpendingAccount(1, money);
                    bank.addAccount(p, acc);
                } else if (type.equals("saving")) {
                    SavingAccount acc = new SavingAccount(1, money);
                    bank.addAccount(p, acc);
                } else {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addModifyPersonListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setHeader("Modify person with given ID");
                frame.buildModifyPerson();
            }
        });

        frame.addModifyPersonConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int id;
                String name;

                try {
                    id = Integer.valueOf(frame.txtPersonID.getText());
                    name = frame.txtPersonName.getText();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == id) {
                        bank.getPersons().get(i).setName(name);
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addDeletePersonListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("Delete person with given ID");
                frame.buildDeletePerson();
            }
        });

        frame.addDeletePersonConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int id;

                try {
                    id = Integer.valueOf(frame.txtPersonID.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == id) {
                        bank.deletePerson(bank.getPersons().get(i));
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addAddAccountListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setHeader("Add account for given person");
                frame.buildAddAccount();
            }

        });

        frame.addAddAccountConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int id;
                double money;
                String type;

                try {
                    id = Integer.valueOf(frame.txtPersonID.getText());
                    money = Double.valueOf(frame.txtInitialAmount.getText());
                    type = frame.txtAccountType.getText();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Person p = bank.getPersons().get(0);
                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == id) {
                        p = bank.getPersons().get(i);
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int accID = bank.getMaxAccIDForPerson(p) + 1;

                if (type.equals("spending")) {
                    System.out.println(accID + "      " + money);
                    SpendingAccount acc = new SpendingAccount(accID, money);
                    bank.addAccount(p, acc);
                } else if (type.equals("saving")) {
                    SavingAccount acc = new SavingAccount(accID, money);
                    bank.addAccount(p, acc);
                } else {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addModifyAccountListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setHeader("Modify an account of a given person");
                frame.buildModifyAccount();
            }

        });

        frame.addModifyAccountConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int personID, accID;
                double money;
                String type;

                try {
                    personID = Integer.valueOf(frame.txtPersonID.getText());
                    accID = Integer.valueOf(frame.txtAccountID.getText());
                    money = Double.valueOf(frame.txtInitialAmount.getText());
                    type = frame.txtAccountType.getText();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Person p = bank.getPersons().get(0);
                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == personID) {
                        p = bank.getPersons().get(i);
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                i = 0;
                Set<Account> accounts = bank.getAccounts(p);
                for (Account a : accounts) {
                    if (a.getAccountID() == accID) {
                        boolean isSavingAccount = false;

                        if (a instanceof SavingAccount) {
                            isSavingAccount = true;
                        }

                        if (type.equals("spending") && !isSavingAccount) {
                            a.setMoney(money);
                        } else if (type.equals("saving") && isSavingAccount) {
                            a.setMoney(money);
                        } else if (type.equals("saving") && !isSavingAccount) {
                            SavingAccount newAcc = new SavingAccount(a.getAccountID(), money);
                            accounts.remove(a);
                            accounts.add(newAcc);
                        } else if (type.equals("spending") && isSavingAccount) {
                            SpendingAccount newAcc = new SpendingAccount(a.getAccountID(), money);
                            accounts.remove(a);
                            accounts.add(newAcc);
                        } else {
                            JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
                                    JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        break;
                    }
                    i++;
                }

                if (i == accounts.size()) {
                    JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addDeleteAccountListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("Delete an account of a given person");
                frame.buildDeleteAccount();
            }
        });

        frame.addDeleteAccountConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int personID, accID;

                try {
                    personID = Integer.valueOf(frame.txtPersonID.getText());
                    accID = Integer.valueOf(frame.txtAccountID.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data lol", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Person p = bank.getPersons().get(0);
                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == personID) {
                        p = bank.getPersons().get(i);
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Set<Account> accounts = bank.getAccounts(p);
                i = accounts.size();
                bank.deleteAccount(accID, p);
                accounts = bank.getAccounts(p);
                i -= accounts.size();

                if (i == 0) {
                    JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addDepositListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("Deposit money in an account of a given person");
                frame.buildDepositMoney();
            }

        });

        frame.addDepositMoneyConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int personID, accID;
                double money;

                try {
                    personID = Integer.valueOf(frame.txtPersonID.getText());
                    accID = Integer.valueOf(frame.txtAccountID.getText());
                    money = Double.valueOf(frame.txtInitialAmount.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data lol", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Person p = bank.getPersons().get(0);
                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == personID) {
                        p = bank.getPersons().get(i);
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                i = 0;
                Set<Account> accounts = bank.getAccounts(p);
                for (Account a : accounts) {
                    if (a.getAccountID() == accID) {
                        bank.depositMoney(money, accID, p);
                        break;
                    }
                    i++;
                }

                if (i == accounts.size()) {
                    JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        frame.addWithdrawMoneyListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHeader("Withdraw money from an account of a given person");
                frame.buildWithdrawMoney();
            }

        });

        frame.addWithdrawMoneyConfirmListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                int personID, accID;
                double money;

                try {
                    personID = Integer.valueOf(frame.txtPersonID.getText());
                    accID = Integer.valueOf(frame.txtAccountID.getText());
                    money = Double.valueOf(frame.txtInitialAmount.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data lol", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Person p = bank.getPersons().get(0);
                int i;
                for (i = 0; i < bank.getPersons().size(); i++) {
                    if (bank.getPersons().get(i).getPersonID() == personID) {
                        p = bank.getPersons().get(i);
                        break;
                    }
                }

                if (i == bank.getPersons().size()) {
                    JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                i = 0;
                Set<Account> accounts = bank.getAccounts(p);
                for (Account a : accounts) {
                    if (a.getAccountID() == accID) {
                        bank.withdrawMoney(money, accID, p);
                        break;
                    }
                    i++;
                }

                if (i == accounts.size()) {
                    JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                FileHandler.writeInformation(bank.getBank(), "bank.txt");
            }
        });

        System.out.println(bank);

    }
}
