package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import model.Account;
import model.Person;

public class FileHandler {

    public static void writeInformation(Map<Person, Set<Account>> data, String filename) {
        try {
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream object = new ObjectOutputStream(file);
            object.writeObject(data);
            object.close();
            file.close();
        } catch (Exception e) { // More generalized catch in order to use only
            // one try-catch block
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static Map<Person, Set<Account>> readInformation(String filename) {
        try {
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream object = new ObjectInputStream(file);
            @SuppressWarnings("unchecked")
            Map<Person, Set<Account>> data = (HashMap<Person, Set<Account>>) object.readObject();
            object.close();
            file.close();

            return data;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

}
