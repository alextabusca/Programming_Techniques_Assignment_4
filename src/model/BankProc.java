package model;

import java.util.List;
import java.util.Set;

public interface BankProc {

    /**
     * @pre person != null and account != null
     * @post ??? initialNumberOfAccounts = finalNumberOfAccounts - 1
     * @param person The owner of the account to be added
     * @param account The account that we'll add
     */
    public void addAccount(Person person, Account account);


    /**
     * @pre person != null && amount >=0 (&& account != null)
     * @post initialAmount = finalAmount - amount
     * @param amount
     * @param accountID
     * @param person
     */
    public void depositMoney(double amount, int accountID, Person person);


    /**
     * @pre person != null && amount >= 0 (&& account != null)
     * @post initialAmount = finalAmount + amount
     * @param amount
     * @param accountID
     * @param person
     */
    public void withdrawMoney(double amount, int accountID, Person person);


    /**
     * @pre person != null && account != null
     * @post initialNumberOfAccounts = finalNumberOfAccounts + 1
     * @param accountID
     * @param person
     */
    public void deleteAccount(int accountID, Person person);


    /**
     * @pre person != null && person is in the bank
     * @post ???
     * @param person
     */
    public void deletePerson(Person person);


    /**
     * @pre ???
     * @post no change
     * @return
     */
    public  List<Person> getPersons();


    /**
     * @pre
     * @post no change
     * @param person
     * @return
     */
    public Set<Account> getAccounts(Person person);

}
