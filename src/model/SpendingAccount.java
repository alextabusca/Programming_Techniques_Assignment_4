package model;

public class SpendingAccount extends Account {

    private static final long serialVersionUID = 4892774595141857856L;

    public SpendingAccount(int accountID, double money) {
        super(accountID, money);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void depositMoney(double money) {
        // TODO Auto-generated method stub
        setMoney(getMoney() + money);
        setChanged();
        notifyObservers(money);
    }

    @Override
    public void withdrawMoney(double money) {
        // TODO Auto-generated method stub
        setMoney(getMoney() - money);
        setChanged();
        notifyObservers(money);
    }

}
