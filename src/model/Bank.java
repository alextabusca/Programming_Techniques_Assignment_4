package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Bank implements BankProc {

    private Map<Person, Set<Account>> bank;

    // Constructor
    public Bank() {
        bank = new HashMap<Person, Set<Account>>();
    }

    /**
     * Invariant method that is used to verify whether the bank is in the
     * correct format
     *
     * @return True if correct format, False otherwise
     */
    public boolean isWellFormed() {
        for (Entry<Person, Set<Account>> entry : bank.entrySet())
            if (entry.getValue() == null || entry.getValue().isEmpty())
                return false;

        return true;
    }

    @Override
    public String toString() {
        return "Bank: [bank=" + bank + "]";
    }

    @Override
    public void addAccount(Person person, Account account) {
        // TODO Auto-generated method stub
        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";
        assert person != null : "Person must not be NULL!";

        int initialNumber;
        if (bank.containsKey(person)) {
            initialNumber = bank.get(person).size();
            bank.get(person).add(account);
        } else {
            Set<Account> accounts = new HashSet<Account>();
            initialNumber = 0;
            accounts.add(account);
            bank.put(person, accounts);
        }

        account.addObserver(person);

        int finalNumber = bank.get(person).size();
        assert initialNumber == finalNumber - 1 : "Account can't be added!";

        wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";
    }

    @Override
    public void depositMoney(double amount, int accountID, Person person) {
        // TODO Auto-generated method stub
        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";

        assert person != null : "Person must not be null";
        assert bank.containsKey(person);
        assert amount >= 0 : "The amount has to be greater than 0!";

        double rate = 0;
        double initialAmount = 0, finalAmount = 1;
        boolean isSavingAccount = false;

        if (bank.containsKey(person)) {
            Set<Account> accounts = bank.get(person);
            for (Account acc : accounts) {
                initialAmount = acc.getMoney();
                acc.depositMoney(amount);
                finalAmount = acc.getMoney();

                if (acc instanceof SavingAccount) {
                    isSavingAccount = true;
                    rate = ((SavingAccount) acc).getInterestRate();
                }
            }
        }

        if (isSavingAccount)
            assert initialAmount == (finalAmount - amount
                    - amount * rate) : "The specified amount could not pe deposited!";
        else
            assert initialAmount == finalAmount - amount : "The specified amount could not be deposited";

        wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";

    }

    @Override
    public void withdrawMoney(double amount, int accountID, Person person) {
        // TODO Auto-generated method stub
        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";
        assert person != null : "Person must not be null!";
        assert bank.containsKey(person) : "The person must be from the database!";
        assert amount >= 0 : "The amount has to be greater than 0!";

        double initialAmount = 0, finalAmount = 1;
        boolean isSavingAccount = false;
        double rate = 0;
        if (bank.containsKey(person)) {
            Set<Account> accounts = bank.get(person);
            for (Account acc : accounts) {
                if (acc.getAccountID() == accountID) {
                    initialAmount = acc.getMoney();
                    acc.withdrawMoney(amount);
                    finalAmount = acc.getMoney();
                    if (acc instanceof SavingAccount) {
                        isSavingAccount = true;
                        rate = ((SavingAccount) acc).getInterestRate();
                    }
                }
            }
        }

        if (isSavingAccount) {
            assert initialAmount == finalAmount + amount
                    + amount * rate * 2 : "The specified amount could not be withdrawn!";
        } else {
            assert initialAmount == finalAmount + amount : "The specified amount could not be withdrawn!";
        }

        wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";

    }

    @Override
    public void deleteAccount(int accountID, Person person) {
        // TODO Auto-generated method stub
        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well Formed";

        if (bank.containsKey(person)) {
            Set<Account> accounts = bank.get(person);
            for (Account acc : accounts) {
                if (acc.getAccountID() == accountID) {
                    accounts.remove(acc);
                    break;
                }
            }
        }

        wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";
    }

    @Override
    public void deletePerson(Person person) {
        // TODO Auto-generated method stub

        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed!";

        bank.remove(person);

        wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed";
    }

    @Override
    public List<Person> getPersons() {
        // TODO Auto-generated method stub
        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank not well formed";
        assert !bank.keySet().isEmpty() : "There are no persons in the database";

        List<Person> resultList = new ArrayList<Person>();
        for (Person person : bank.keySet()) {
            resultList.add(person);
        }

        wellFormed = isWellFormed();
        assert wellFormed : "Bank not well formed";
        return resultList;
    }

    @Override
    public Set<Account> getAccounts(Person person) {
        // TODO Auto-generated method stub
        boolean wellFormed = isWellFormed();
        assert wellFormed : "Bank is not well formed";

        assert person != null : "Person must not be null!";
        assert bank.containsKey(person) : "Person must be in the database";

        return bank.get(person);
    }

    public void setData(Map<Person, Set<Account>> bank) {
        this.bank = bank;
    }

    public Map<Person, Set<Account>> getBank() {
        return bank;
    }

    public int getMaxAccIDForPerson(Person person) {
        // TODO Auto-generated method stub
        int id = 0;
        for (Account acc : getAccounts(person)) {
            if (id < acc.getAccountID())
                id = acc.getAccountID();
        }
        return id;
    }

    public int getMaxPersonID() {
        // TODO Auto-generated method stub
        int id = 0;
        for (Person p : getPersons()) {
            if (id < p.getPersonID())
                id = p.getPersonID();
        }
        return id;
    }

}
