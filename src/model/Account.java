package model;

import java.io.Serializable;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable {

    private static final long serialVersionUID = 6370511787240156610L;
    private int accountID;
    private double money;

    public Account(int accountID, double money) {
        super();
        this.accountID = accountID;
        this.money = money;
    }

    /**
     * Deposits a given sum in the account.
     *
     * @param money
     *            The sum to be deposited.
     */
    public abstract void depositMoney(double money);

    /**
     * Withdraws a given sum from the account.
     *
     * @param money
     *            The sum to be withdrawn.
     */
    public abstract void withdrawMoney(double money);

    @Override
    public int hashCode() {
        int result;
        result = 37 + accountID;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Account other = (Account) obj;
        if (accountID != other.accountID)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Account [accID=" + accountID + ", money=" + money + "]";
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

}
