package model;

public class SavingAccount extends Account {

    private static final long serialVersionUID = 6579180403957731651L;
    private double rate = 0.1;

    public SavingAccount(int accountID, double money) {
        super(accountID, money);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void depositMoney(double money) {
        // TODO Auto-generated method stub
        setMoney(getMoney() + money + money * rate);
        setChanged();
        notifyObservers(money);
    }

    @Override
    public void withdrawMoney(double money) {
        // TODO Auto-generated method stub
        setMoney(getMoney() - money - money * rate * 2);
        setChanged();
        notifyObservers(money);
    }

    public double getInterestRate() {
        return rate;
    }

    public void setInterestRate(double rate) {
        this.rate = rate;
    }

}
