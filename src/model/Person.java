package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer, Serializable {

    private static final long serialVersionUID = 1613413607550535211L;
    private String name;
    private int personID;

    public Person(String name, int personID) {
        super();
        this.name = name;
        this.personID = personID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    // Hashing
    @Override
    public int hashCode() {
        int result = 1;

        if (name != null) {
            result = 37 + name.hashCode();
        }

        result = 37 * result + personID;
        return result;
    }

    // Observable
    @Override
    public void update(Observable arg0, Object arg1) {
        if (arg0 instanceof Account) {
            Account acc = (Account) arg0;
            System.out.println("Observer update: " + acc.getAccountID() + " " + arg1.toString());
        }

    }

    //Equality between account type objects
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (personID != other.personID)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Person-[name=" + name + ", personID=" + personID + "]";
    }

}
