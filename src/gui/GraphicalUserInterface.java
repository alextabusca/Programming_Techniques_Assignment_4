package gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Account;
import model.Person;
import model.SpendingAccount;

public class GraphicalUserInterface extends JFrame {

    private static final long serialVersionUID = 7918316329230708052L;

    JPanel informationPanel = new JPanel();
    JPanel buttonPanel = new JPanel();

    private JButton btnListPersons = new JButton("List All Persons");
    private JButton btnListAccounts = new JButton("List Accounts");
    private JButton btnAddPerson = new JButton("Add Person");
    private JButton btnModifyPerson = new JButton("Modify Person");
    private JButton btnDeletePerson = new JButton("Delete Person");
    private JButton btnAddAccount = new JButton("Add Account");
    private JButton btnModifyAccount = new JButton("Modify Account");
    private JButton btnDeleteAccount = new JButton("Delete Account");
    private JButton btnDepositMoney = new JButton("Deposit");
    private JButton btnWithdrawMoney = new JButton("Withdraw");

    private JLabel lblHeader = new JLabel();

    public JTextField txtPersonID = new JTextField();
    private JButton btnAccountList = new JButton("Confirm");

    JPanel accountPanel = new JPanel();
    JPanel accListPanel = new JPanel();

    public JTextField txtPersonName = new JTextField();
    public JTextField txtInitialAmount = new JTextField();
    public JTextField txtAccountType = new JTextField();
    public JTextField txtAccountID = new JTextField();

    private JButton btnConfirmPersonAdd = new JButton("Confirm");
    private JButton btnConfirmPersonModify = new JButton("Confirm");
    private JButton btnConfirmPersonDelete = new JButton("Confirm");
    private JButton btnConfirmAccountAdd = new JButton("Confirm");
    private JButton btnConfirmAccountModify = new JButton("Confirm");
    private JButton btnConfirmAccountDelete = new JButton("Confirm");
    private JButton btnConfirmWithdrawMoney = new JButton("Confirm");
    private JButton btnConfirmDepositMoney = new JButton("Confirm");

    public GraphicalUserInterface() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setTitle("Bank Simulator");
        setSize(750, 500);
        setLocationRelativeTo(null);

        buttonPanel.setLayout(new GridLayout(5, 2));
        buttonPanel.add(btnListPersons);
        buttonPanel.add(btnListAccounts);
        buttonPanel.add(btnAddPerson);
        buttonPanel.add(btnModifyPerson);
        buttonPanel.add(btnDeletePerson);
        buttonPanel.add(btnAddAccount);
        buttonPanel.add(btnModifyAccount);
        buttonPanel.add(btnDeleteAccount);
        buttonPanel.add(btnDepositMoney);
        buttonPanel.add(btnWithdrawMoney);
        this.add(buttonPanel, BorderLayout.WEST);

        informationPanel.setLayout(new BorderLayout());
        informationPanel.add(new JPanel(), BorderLayout.WEST);
        this.add(informationPanel, BorderLayout.CENTER);

        setVisible(true);
    }

    public void buildPersonList(List<Person> list) {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel personList = new JPanel();
        personList.setLayout(new GridLayout(0, 1));
        for (int i = 0; i < list.size(); i++) {
            personList.add(buildPersonPanel(list.get(i)));
        }
        informationPanel.add(personList, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public JPanel buildPersonPanel(Person p) {
        JPanel resultPanel = new JPanel();
        resultPanel.setLayout(new GridLayout(1, 3));
        resultPanel.add(new JLabel("ID: " + p.getPersonID()));
        resultPanel.add(new JLabel("Name: " + p.getName()));
        return resultPanel;
    }

    public void buildAccountList() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        accountPanel.setLayout(new BorderLayout());

        JPanel head = new JPanel();
        head.setLayout(new GridLayout(1, 4));
        head.add(new JLabel("personID: "));
        head.add(txtPersonID);
        head.add(btnAccountList);
        head.add(new JPanel());
        accountPanel.add(head, BorderLayout.NORTH);

        accListPanel.setLayout(new GridLayout(0, 3));
        accListPanel.removeAll();
        accountPanel.add(accListPanel, BorderLayout.CENTER);

        informationPanel.add(accountPanel, BorderLayout.CENTER);

        accountPanel.revalidate();
        accountPanel.repaint();
        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildAccountPanel(Account acc) {
        accListPanel.add(new JLabel("ID: " + acc.getAccountID()));
        accListPanel.add(new JLabel("Money: " + acc.getMoney()));
        String type;
        if (acc instanceof SpendingAccount) {
            type = "spending";
        } else {
            type = "saving";
        }
        accListPanel.add(new JLabel("Type: " + type));
        accListPanel.revalidate();
        accListPanel.repaint();
        accountPanel.revalidate();
        accountPanel.repaint();
        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildAddPerson() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel addPanel = new JPanel();
        addPanel.setLayout(new GridLayout(6, 2));

        addPanel.add(new JLabel("Name: "));
        addPanel.add(txtPersonName);
        addPanel.add(new JLabel("Starting money: "));
        addPanel.add(txtInitialAmount);
        addPanel.add(new JLabel("Account type: "));
        addPanel.add(txtAccountType);
        addPanel.add(new JPanel());
        addPanel.add(btnConfirmPersonAdd);

        informationPanel.add(addPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildModifyPerson() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel modPanel = new JPanel();
        modPanel.setLayout(new GridLayout(4, 2));

        modPanel.add(new JLabel("ID: "));
        modPanel.add(txtPersonID);
        modPanel.add(new JLabel("New name: "));
        modPanel.add(txtPersonName);

        modPanel.add(new JPanel());
        modPanel.add(btnConfirmPersonModify);

        informationPanel.add(modPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildDeletePerson() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel deletePanel = new JPanel();
        deletePanel.setLayout(new GridLayout(4, 2));

        deletePanel.add(new JLabel("ID: "));
        deletePanel.add(txtPersonID);

        deletePanel.add(new JPanel());
        deletePanel.add(btnConfirmPersonDelete);

        deletePanel.add(new JPanel());
        deletePanel.add(new JPanel());

        informationPanel.add(deletePanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildAddAccount() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel addPanel = new JPanel();
        addPanel.setLayout(new GridLayout(6, 2));

        addPanel.add(new JLabel("Person ID: "));
        addPanel.add(txtPersonID);
        addPanel.add(new JLabel("Starting money: "));
        addPanel.add(txtInitialAmount);
        addPanel.add(new JLabel("Account type: "));
        addPanel.add(txtAccountType);
        addPanel.add(new JPanel());
        addPanel.add(btnConfirmAccountAdd);

        informationPanel.add(addPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildModifyAccount() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel addPanel = new JPanel();
        addPanel.setLayout(new GridLayout(6, 2));

        addPanel.add(new JLabel("Person ID: "));
        addPanel.add(txtPersonID);
        addPanel.add(new JLabel("Account ID: "));
        addPanel.add(txtAccountID);
        addPanel.add(new JLabel("New money: "));
        addPanel.add(txtInitialAmount);
        addPanel.add(new JLabel("New account type: "));
        addPanel.add(txtAccountType);
        addPanel.add(new JPanel());
        addPanel.add(btnConfirmAccountModify);

        informationPanel.add(addPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildDeleteAccount() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel addPanel = new JPanel();
        addPanel.setLayout(new GridLayout(6, 2));

        addPanel.add(new JLabel("Person ID: "));
        addPanel.add(txtPersonID);
        addPanel.add(new JLabel("Account ID: "));
        addPanel.add(txtAccountID);
        addPanel.add(new JPanel());
        addPanel.add(btnConfirmAccountDelete);
        addPanel.add(new JPanel());
        addPanel.add(new JPanel());
        addPanel.add(new JPanel());
        addPanel.add(new JPanel());

        informationPanel.add(addPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildDepositMoney() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel addPanel = new JPanel();
        addPanel.setLayout(new GridLayout(6, 2));

        addPanel.add(new JLabel("Person ID: "));
        addPanel.add(txtPersonID);
        addPanel.add(new JLabel("Account ID: "));
        addPanel.add(txtAccountID);
        addPanel.add(new JLabel("Amount: "));
        addPanel.add(txtInitialAmount);
        addPanel.add(new JPanel());
        addPanel.add(btnConfirmDepositMoney);

        informationPanel.add(addPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void buildWithdrawMoney() {
        informationPanel.removeAll();
        informationPanel.revalidate();
        informationPanel.add(lblHeader, BorderLayout.NORTH);

        JPanel addPanel = new JPanel();
        addPanel.setLayout(new GridLayout(6, 2));

        addPanel.add(new JLabel("Person ID: "));
        addPanel.add(txtPersonID);
        addPanel.add(new JLabel("Account ID: "));
        addPanel.add(txtAccountID);
        addPanel.add(new JLabel("Amount: "));
        addPanel.add(txtInitialAmount);
        addPanel.add(new JPanel());
        addPanel.add(btnConfirmWithdrawMoney);

        informationPanel.add(addPanel, BorderLayout.CENTER);

        informationPanel.revalidate();
        informationPanel.repaint();
    }

    public void setHeader(String text) {
        lblHeader.setText(text);
    }

    public void addDeleteAccountConfirmListener(ActionListener a) {
        btnConfirmAccountDelete.addActionListener(a);
    }

    public void addModifyAccountConfirmListener(ActionListener a) {
        btnConfirmAccountModify.addActionListener(a);
    }

    public void addDepositMoneyConfirmListener(ActionListener a) {
        btnConfirmDepositMoney.addActionListener(a);
    }

    public void addWithdrawMoneyConfirmListener(ActionListener a) {
        btnConfirmWithdrawMoney.addActionListener(a);
    }

    public void addAddAccountConfirmListener(ActionListener a) {
        btnConfirmAccountAdd.addActionListener(a);
    }

    public void addDeletePersonConfirmListener(ActionListener a) {
        btnConfirmPersonDelete.addActionListener(a);
    }

    public void addModifyPersonConfirmListener(ActionListener a) {
        btnConfirmPersonModify.addActionListener(a);
    }

    public void addAddPersonConfirmListener(ActionListener a) {
        btnConfirmPersonAdd.addActionListener(a);
    }

    public void addListAccountsConfirmListener(ActionListener a) {
        btnAccountList.addActionListener(a);
    }

    public void addListPersonsListener(ActionListener a) {
        btnListPersons.addActionListener(a);
    }

    public void addListAccountsListener(ActionListener a) {
        btnListAccounts.addActionListener(a);
    }

    public void addAddPersonListener(ActionListener a) {
        btnAddPerson.addActionListener(a);
    }

    public void addModifyPersonListener(ActionListener a) {
        btnModifyPerson.addActionListener(a);
    }

    public void addDeletePersonListener(ActionListener a) {
        btnDeletePerson.addActionListener(a);
    }

    public void addAddAccountListener(ActionListener a) {
        btnAddAccount.addActionListener(a);
    }

    public void addModifyAccountListener(ActionListener a) {
        btnModifyAccount.addActionListener(a);
    }

    public void addDeleteAccountListener(ActionListener a) {
        btnDeleteAccount.addActionListener(a);
    }

    public void addDepositListener(ActionListener a) {
        btnDepositMoney.addActionListener(a);
    }

    public void addWithdrawMoneyListener(ActionListener a) {
        btnWithdrawMoney.addActionListener(a);
    }
}
